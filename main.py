from tkinter.constants import END
import pygame
from pygame import mouse
from pygame.constants import USEREVENT
pygame.init()
from math import sin, pi
from tkinter import filedialog
import tkinter as tk
tk.Tk().withdraw()
import os

#DIR = "."
#DIR=os.getcwd()
DIR=os.path.abspath(os.path.dirname(__file__))

SCREENWIDTH = 1280
SCREENHEIGHT = 720
MAXSONGLENGTH = 960  #maximum number of time positions in a song
NOTERANGE = 16  #maximum range of notes
NOTESPACING_Y = 18
PADDING_X = 200
PADDING_Y = 200
MAXNOTESPERCELL = 4
NOTESIZE = 25
NOTEBUTTON_X = 300
NOTEBUTTON_Y = 50
NOTEBUTTONSPACING = 50
ENDPOINT = -1  #the endpoint of the song
#CURRENT_TIMEPOINT = 0
BPM = 200
SIGNATURE = 4 #4 means 4/4, 3 means 3/4
LOOPING = False
MAXZOOM = 10
MINZOOM = 2
DEFAULTZOOM = 4
BOUNCE_TIME = 180 #milliseconds
BOUNCE_HEIGHT = 10 #pixels
NUMCHANNELS = 64

ADVANCE_BEAT = USEREVENT+1
PLAY_NOTES = USEREVENT+2


#assert(MAXSONGLENGTH%8 == 0)

pygame.font.init()
myfont = pygame.font.SysFont('Arial', 25)
bpm_font = pygame.font.SysFont('Arial', 60)
clock = pygame.time.Clock()

note_names = [
    "C6", "B5", "A5", "G5", "F5", "E5", "D5", "C5", "B4", "A4", "G4", "F4", "E4", "D4", "C4", "B3"
]

note_sprites = [
    DIR+"/gfx/note_icons/luz.png",
    DIR+"/gfx/note_icons/king.png",
    DIR+"/gfx/note_icons/eda.png",
    DIR+"/gfx/note_icons/hooty.png",
    DIR+"/gfx/note_icons/owlbert.png",
    DIR+"/gfx/note_icons/amity.png",   
    DIR+"/gfx/note_icons/lilith.png",
    DIR+"/gfx/note_icons/willow.png",
    DIR+"/gfx/note_icons/gus.png",
    DIR+"/gfx/note_icons/raine.png",
    DIR+"/gfx/note_icons/kikimora.png",
    DIR+"/gfx/note_icons/hunter.png",
    DIR+"/gfx/note_icons/belos.png",
    DIR+"/gfx/note_icons/vee.png",
    DIR+"/gfx/note_icons/ghost.png",
    DIR+"/gfx/note_icons/lego_eda.png"
]

logo = pygame.image.load(DIR+"/gfx/objects/logo.png")
pygame.display.set_icon(logo)

def load_sounds(folder):
    sounds = []
    #Normal notes
    for i in range(len(note_names)):
        sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+note_names[i]+".wav"))
    #Sharp/flat notes
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"C#6.wav"))
    sounds.append(None)
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"A#5.wav"))
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"G#5.wav"))
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"F#5.wav"))
    sounds.append(None)
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"D#5.wav"))
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"C#5.wav"))
    sounds.append(None)
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"A#4.wav"))
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"G#4.wav"))
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"F#4.wav"))
    sounds.append(None)
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"D#4.wav"))
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"C#4.wav"))
    sounds.append(None)
    sounds.append(pygame.mixer.Sound(DIR+"/notes/"+folder+"/"+"A#3.wav"))
    return sounds


sound_catalog = []

sound_catalog.append(load_sounds("luz")) 
sound_catalog.append(load_sounds("king"))
sound_catalog.append(load_sounds("eda"))
sound_catalog.append(load_sounds("hooty"))
sound_catalog.append(load_sounds("owlbert"))
sound_catalog.append(load_sounds("amity"))
sound_catalog.append(load_sounds("lilith"))
sound_catalog.append(load_sounds("willow"))
sound_catalog.append(load_sounds("gus"))
sound_catalog.append(load_sounds("raine"))
sound_catalog.append(load_sounds("kikimora"))
sound_catalog.append(load_sounds("hunter"))
sound_catalog.append(load_sounds("belos"))
sound_catalog.append(load_sounds("vee"))
sound_catalog.append(load_sounds("ghost"))

l_sounds = []
for i in range(len(sound_catalog[0])):
    l_sounds.append(pygame.mixer.Sound(DIR+"/notes/lego/C5.wav"))
sound_catalog.append(l_sounds)

colors  = {
    "white":(200,255,255),
    "red":(255,0,0),
    "blue":(0,255,0),
    "green":(0,0,255),
    "blah":(215,208,200),
    "brown":(119,112,81),
    "other_brown":(153,51,0),
    "light_gray":(233,233,233),
    "black":(0,0,0),
    "paper_brown":(140,125,95),
    "paper_tan":(200,186,148),
}
id_color = {
    1:"white",
    2:"red",
    3:"blue",
    4:"green",
    5:"blah"
}

def can_be_sharped(row):
    if row in [1,5,8,12,15]:
        return False
    else:
        return True

def can_be_flatted(row):
    if row in [0,4,7,11,14]:
        return False
    else:
        return True

class EndMarker(pygame.sprite.Sprite):
    def __init__(self, color):
        super(EndMarker, self).__init__()
        self.surf = pygame.Surface((20, SCREENHEIGHT-PADDING_Y-267), pygame.SRCALPHA)
        
        thickline = pygame.Surface((5, SCREENHEIGHT-PADDING_Y-267))
        thickline.fill(colors[color])
        thinline = pygame.Surface((2, SCREENHEIGHT-PADDING_Y-267))
        thinline.fill(colors[color])

        self.surf.blit(thickline,(0,0))
        self.surf.blit(thinline,(10,0))
        self.rect = self.surf.get_rect()
        #self.surf.fill(colors[color])
        #self.rect = self.surf.get_rect()
    def goto_timepoint(self, tp, spacing, offset):
        global ENDPOINT, PADDING_X, PADDING_Y
        self.rect.x = PADDING_X + (tp*spacing) - offset
        self.rect.y = PADDING_Y
 

class LoopButton(pygame.sprite.Sprite):
    def __init__(self):
        super(LoopButton, self).__init__()
        self.sprite_unchecked = pygame.image.load(DIR+"/gfx/objects/loop_button_unchecked.png").convert_alpha()
        self.sprite_checked = pygame.image.load(DIR+"/gfx/objects/loop_button_checked.png").convert_alpha()
        self.surf = self.sprite_checked
        self.rect = self.surf.get_rect()
        self.rect.x = SCREENWIDTH-PADDING_X-1050
        self.rect.y = SCREENHEIGHT-PADDING_Y+55
        self.update_appearance()
    def update_appearance(self):
        if LOOPING:
            self.surf = self.sprite_checked
        else:
            self.surf = self.sprite_unchecked

class SignatureButton(pygame.sprite.Sprite):
    def __init__(self):
        super(SignatureButton, self).__init__()
        self.sprite_44 = pygame.image.load(DIR+"/gfx/objects/signature_button_44.png").convert_alpha()
        self.sprite_34 = pygame.image.load(DIR+"/gfx/objects/signature_button_34.png").convert_alpha()
        self.surf = self.sprite_44   
        self.rect = self.surf.get_rect()
        self.update_appearance()
        self.rect.x = SCREENWIDTH-PADDING_X-800
        self.rect.y = SCREENHEIGHT-PADDING_Y+55
    def update_appearance(self):
        if SIGNATURE == 4:
            self.surf = self.sprite_44
        elif SIGNATURE == 3:
            self.surf = self.sprite_34
        else:
            return Exception

class BPMButton(pygame.sprite.Sprite):
    def __init__(self):
        super(BPMButton, self).__init__()
        self.update_appearance()
    def click_increase(self, pos):
        global BPM
        #print(pos)
        rel_x = pos[0] - self.rect.x
        if (60 < rel_x) and (rel_x < 100) and (BPM+100 < 1000):
            BPM+=100
        elif (100 < rel_x) and (rel_x < 135) and (BPM+10 < 1000):
            BPM+=10
        elif (135 < rel_x) and (rel_x < 170) and (BPM+1 < 1000):
            BPM+=1
        self.update_appearance()

    def click_decrease(self, pos):
        global BPM
        rel_x = pos[0] - self.rect.x
        if (60 < rel_x) and (rel_x < 100) and (BPM-100 > 0):
            BPM-=100
        elif (100 < rel_x) and (rel_x < 135) and (BPM-10 > 0):
            BPM-=10
        elif (135 < rel_x) and (rel_x < 170) and (BPM-1 > 0):
            BPM-=1
        self.update_appearance()

    def update_appearance(self):
        self.surf = pygame.image.load(DIR+"/gfx/objects/bpm_button.png").convert_alpha()
        self.rect = self.surf.get_rect()
        self.rect.x = SCREENWIDTH-PADDING_X-550
        self.rect.y = SCREENHEIGHT-PADDING_Y+55
        textsurface = bpm_font.render(self.get_bpm_str(), False, colors["paper_brown"])
        textsurface = pygame.transform.rotate(textsurface, 4)
        self.surf.blit(textsurface, (70,20))
    def get_bpm_str(self):
        global BPM
        return_str = ""
        if BPM < 100:
            return_str += '0'
            if BPM < 10:
                return_str += '0' + str(BPM)
            else:
                return_str += str(BPM)
        else:
            return_str = str(BPM)
        return return_str

class LoadButton(pygame.sprite.Sprite):
    def __init__(self):
        super(LoadButton, self).__init__()
        self.surf = pygame.image.load(DIR+"/gfx/objects/load_button.png").convert_alpha()
        self.rect = self.surf.get_rect()
        self.rect.x = SCREENWIDTH-PADDING_X-50
        self.rect.y = SCREENHEIGHT-PADDING_Y+55

class SaveButton(pygame.sprite.Sprite):
    def __init__(self):
        super(SaveButton, self).__init__()
        self.surf = pygame.image.load(DIR+"/gfx/objects/save_button.png").convert_alpha()
        self.rect = self.surf.get_rect()
        self.rect.x = SCREENWIDTH-PADDING_X-300
        self.rect.y = SCREENHEIGHT-PADDING_Y+55

class Background(pygame.sprite.Sprite):
    def __init__(self):
        super(Background, self).__init__()
        self.surf = pygame.image.load(DIR+"/gfx/objects/background.png").convert_alpha()
        self.rect = self.surf.get_rect()
        

class Scrollbar(pygame.sprite.Sprite):
    def __init__(self):
        super(Scrollbar, self).__init__()

        self.min_x = PADDING_X/2
        self.width = 100
        self.max_x = SCREENWIDTH-self.min_x-self.width

        self.surf = pygame.Surface((self.width, 35))
        self.surf.fill(colors["paper_tan"])
        self.rect = self.surf.get_rect()
        self.goto(self.min_x, 500)
        self.dragged = False

        self.backdrop = pygame.Surface((self.max_x-self.min_x+5+(self.width), 45))
        self.backdrop.fill(colors["paper_brown"])
        self.backdrop_rect = self.backdrop.get_rect()
        self.backdrop_rect.x = self.min_x - 5
        self.backdrop_rect.y = 495
    def goto(self, x, y):
        self.rect.x = x
        self.rect.y = y
    
    def follow_mouse(self):
        mouse_x = pygame.mouse.get_pos()[0] - int(self.width/2)
        if (mouse_x > self.min_x) and (mouse_x < self.max_x):
            self.rect.x = mouse_x
    
    def update_x(self,scroll):
        #print(self.rect.x)
        #print(self.min_x+(PADDING_X-OFFSET_X))
       # dx = ((PADDING_X-offset_x)*(self.max_x-self.min_x))/(MAXSONGLENGTH*spacing)
        #self.goto(self.min_x+dx, 500)
        self.goto(self.min_x + scroll*(self.max_x - self.min_x), 500)

    def get_scroll(self, spacing): 
        #return self.rect.x - self.min_x
        return ((self.rect.x - self.min_x)/(self.max_x - self.min_x))
        


class Flat(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super(Flat, self).__init__()
        self.surf = pygame.image.load(DIR+"/gfx/note_icons/flat.png").convert_alpha()
        self.rect = self.surf.get_rect(center=(x,y))
    def goto(self, x, y):
        self.rect.center = (x,y)

class Sharp(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super(Sharp, self).__init__()
        self.surf = pygame.image.load(DIR+"/gfx/note_icons/sharp.png").convert_alpha()
        self.rect = self.surf.get_rect(center=(x,y))
    def goto(self, x, y):
        self.rect.center = (x,y)


class Treble(pygame.sprite.Sprite):
    def __init__(self):
        super(Treble, self).__init__()
        self.surf = pygame.image.load(DIR+"/gfx/objects/treble_clef.png").convert_alpha()
        self.rect = self.surf.get_rect(center=(100,335))

class Playhead(pygame.sprite.Sprite):
    def __init__(self):
        super(Playhead, self).__init__()
        #self.surf = pygame.Surface((10, SCREENHEIGHT-PADDING_Y-140))
        #self.surf.fill(colors["other_brown"])
        self.default_sprite = pygame.image.load(DIR+"/gfx/objects/playhead.png").convert_alpha()
        self.playing_sprite = pygame.image.load(DIR+"/gfx/objects/playhead_glowing.png").convert_alpha()

        self.surf = self.default_sprite
        self.rect = self.surf.get_rect()
        self.rect.y = PADDING_Y-80
        self.dragged = False
        self.move_timer = 0
    def goto_timepoint(self, tp, spacing, offset_pixels):
        #print(OFFSET_X+(tp*spacing) - 28)
        self.move_timer = 0
        self.rect.x = PADDING_X + (tp*spacing) - offset_pixels - 28
 
    def goto(self, x):
        self.rect.x = x - 28

    def reset_movetimer(self):
        self.move_timer = 0

    def scroll(self, tp, spacing, offset_pixels):
        if tp >= MAXSONGLENGTH:
            return
        time_per_eighth = 60000/(2*BPM)
        self.rect.x = PADDING_X + (tp*spacing) - offset_pixels - 28 + (self.move_timer/time_per_eighth)*(spacing)

class Horizontal_line(pygame.sprite.Sprite):
    def __init__(self,x,y, thickness, color):
        super(Horizontal_line, self).__init__()
        self.surf = pygame.Surface((SCREENWIDTH,thickness))
        self.surf.fill(colors[color])
        self.rect = self.surf.get_rect(center=(x+(SCREENWIDTH/2),y))


class Vertical_line(pygame.sprite.Sprite):
    def __init__(self,x,y,thickness, color):
        self.surf = pygame.Surface((thickness, SCREENHEIGHT-PADDING_Y-267))
        self.surf.fill(colors[color])
        self.rect = self.surf.get_rect()
        #self.x_original = x
        self.rect.x = x
        self.rect.y = y
    #def goto_x(self, x):
        #self.rect.x = self.x_original + x
    def goto(self, x):
        self.rect.x = x


class Note(pygame.sprite.Sprite):
    def __init__(self, id):
        super(Note, self).__init__()
        #self.surf = pygame.Surface((NOTESIZE,NOTESIZE))
        self.surf = pygame.image.load(note_sprites[id]).convert_alpha()
        #self.surf.fill(colors[id_color[id]])
        self.rect = self.surf.get_rect()
        self.id = id
        self.pitch = 0 #    -1 means flat, +1 means sharp
        self.bounce_initial = 0
        self.bounce_timer = 0
        self.bouncing = False
        self.flat = Flat(0,0)
        self.sharp = Sharp(0,0)
    
    def goto(self, x, y):
        self.rect.center = (x,y)

    def goto_mouse(self):
        self.rect.center =  pygame.mouse.get_pos()

    def get_id(self):
        return self.id

    def draw_sharp(self, game):
        self.sharp.goto(self.rect.x+45, self.rect.y+20)
        game.screen.blit(self.sharp.surf, self.sharp.rect)
    
    def draw_flat(self, game):
        self.flat.goto(self.rect.x+45, self.rect.y+20)
        game.screen.blit(self.flat.surf, self.flat.rect)
    
class GameManager():
    #Initializes the game manager object.
    def __init__(self, screen):
        super(GameManager, self).__init__()
        self.screen = screen
        self.background = Background()
        self.zoom = DEFAULTZOOM
        self.scroll = 0.0
        self.note_buttons = []
        self.dragged_sprites = []
        self.song_grid = []   #This is the structure which holds the data of our song.
        self.measure_lines = []
        self.scale_lines = []
        self.create_scale_lines()
        self.create_measure_lines()
        self.create_note_buttons()
        self.create_song_grid()
        self.current_timepoint = 0
        self.playing = False
        self.treble = Treble()
        self.playhead = Playhead()
        self.playhead.goto_timepoint(0, self.get_note_spacing(), (self.scroll*self.get_total_pixels()))
        self.scrollbar = Scrollbar()
        self.loadbutton = LoadButton()
        self.savebutton = SaveButton()
        self.bpm_button = BPMButton()
        self.sig_button = SignatureButton()
        self.loop_button = LoopButton()
        self.end_marker = None
        self.menu_open = False

    #--------------BUILDING DEFAULT GAME OBJECTS-------------------#
    def create_note_buttons(self):
        for id in range(len(note_sprites)-1):
            note_btn = Note(id)
            note_btn.goto(NOTEBUTTON_X+(id-1)*NOTEBUTTONSPACING, NOTEBUTTON_Y)
            self.note_buttons.append(note_btn)
        #fs_btn = FlatSharpButton()
        #fs_btn.goto(NOTEBUTTON_X+(5)*NOTEBUTTONSPACING, NOTEBUTTON_Y-25)
        #self.note_buttons.append(fs_btn)
    
    def create_song_grid(self):
        for i in range(MAXSONGLENGTH+1):
            timepoint = []
            for j in range(NOTERANGE):
                pitch_notes = []
                #dummy_note = Note(5)
                #pitch_notes.append(dummy_note)
                timepoint.append(pitch_notes)
            self.song_grid.append(timepoint)
    
    def reset_song_grid(self):
        self.song_grid.clear()
        self.create_song_grid()

    def create_scale_lines(self):
        for j in range(NOTERANGE):
            thickness = 2
            if j%2 != 0:
                continue
            elif j in [4,6,8,10,12]:
                thickness = 5
            line = Horizontal_line(0, PADDING_Y+j*NOTESPACING_Y, thickness, "paper_brown")
            self.scale_lines.append(line)
    
    def create_measure_lines(self):
        spacing_x = self.get_note_spacing()
        pixels = self.get_total_pixels()
        for i in range(int(MAXSONGLENGTH/8) + 1):
            #line = Vertical_line(self.offset_x+(i*4)*spacing_x, PADDING_Y, 2, "paper_brown")
            line = Vertical_line(PADDING_X + (i*(2*SIGNATURE))*spacing_x - (self.scroll*pixels), PADDING_Y, 2, "paper_brown")
            self.measure_lines.append(line)

    #---------------------SCREEN REDRAW ROUTINE-----------------------#
    def redraw_screen(self):
        global PADDING_Y, PADDING_X
        spacing_x = self.get_note_spacing()
        pixels = self.get_total_pixels()
        pos = pygame.mouse.get_pos()
        #DRAWING THE BACKGROUND
        #self.screen.fill((255, 204, 153))
        self.screen.blit(self.background.surf, self.background.rect)
       

        #DRAW THE SAVE AND LOAD BUTTONS
        self.screen.blit(self.loadbutton.surf, self.loadbutton.rect)
        if self.loadbutton.rect.collidepoint(pos) or self.savebutton.rect.collidepoint(pos):
            pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_HAND)
        elif self.bpm_button.rect.collidepoint(pos) or self.sig_button.rect.collidepoint(pos):
            pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_HAND)
        elif self.loop_button.rect.collidepoint(pos):
            pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_HAND)
        else: pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)

        if self.dragged_sprites:
            pygame.mouse.set_visible(False)
        else:
            pygame.mouse.set_visible(True)

        self.screen.blit(self.savebutton.surf, self.savebutton.rect)
        self.screen.blit(self.sig_button.surf, self.sig_button.rect)
        self.screen.blit(self.loop_button.surf, self.loop_button.rect)

        if self.scrollbar.dragged:    #The scrollbar at the bottom of the screen
            self.scrollbar.follow_mouse()
            self.scroll = self.scrollbar.get_scroll(spacing_x)
            self.playhead.goto_timepoint(self.current_timepoint, spacing_x, self.scroll*pixels)
        self.screen.blit(self.scrollbar.backdrop, self.scrollbar.backdrop_rect)
        self.screen.blit(self.scrollbar.surf, self.scrollbar.rect)

        self.screen.blit(self.bpm_button.surf, self.bpm_button.rect)

        #DRAWING THE SCALE AND MEASURE LINES
        #for s in self.measure_lines:
            #s.goto_x(self.offset_x-PADDING_X*)
            #self.screen.blit(s.surf, s.rect)
        for i in range(len(self.measure_lines)):
            s = self.measure_lines[i]
            #s.goto(self.offset_x+(i*4)*spacing_x)
            s.goto(PADDING_X + (i*(2*SIGNATURE))*spacing_x - (self.scroll*pixels))
            self.screen.blit(s.surf, s.rect)
        if self.end_marker:
            self.end_marker.goto_timepoint(ENDPOINT, spacing_x, self.scroll*pixels)
            self.screen.blit(self.end_marker.surf, self.end_marker.rect)

        for s in self.scale_lines:
            self.screen.blit(s.surf, s.rect)
        self.screen.blit(self.treble.surf, self.treble.rect)
        
        #DRAWING THE NOTEBUTTONS AT THE TOP OF THE SCREEN
        for s in self.note_buttons:
            self.screen.blit(s.surf, s.rect)
        
        #DRAWING THE PLAYHEAD
        if self.playing:
            self.playhead.move_timer += clock.get_time()
            #elf.playhead.scroll(self.current_timepoint, spacing_x, self.zoom*pixels)
            self.playhead.scroll(self.current_timepoint, self.get_note_spacing(), self.scroll*pixels)
            self.screen.blit(self.playhead.playing_sprite, self.playhead.rect)
        else:
            self.screen.blit(self.playhead.default_sprite, self.playhead.rect)
            
        #DRAWING THE SONG GRID
        for i in range(len(self.song_grid)):
            for j in range(len(self.song_grid[i])):
                for k in range(len(self.song_grid[i][j])):
                    s = self.song_grid[i][j][k]
                    if s.bouncing:              #This bit of code makes the notes "bounce" when they're played.
                        s.bounce_timer += clock.get_time() 
                        if s.bounce_timer >= BOUNCE_TIME:
                            s.bouncing = False
                            s.bounce_timer = 0
                    #s.goto(self.offset_x+i*spacing_x, PADDING_Y+j*NOTESPACING_Y+10*k + int(BOUNCE_HEIGHT*math.sin(math.pi*s.bounce_timer/BOUNCE_TIME)))
                    s.goto(PADDING_X + (i*spacing_x)-(self.scroll * pixels), PADDING_Y+j*NOTESPACING_Y + int(BOUNCE_HEIGHT*sin(pi*s.bounce_timer/BOUNCE_TIME)))
                    self.screen.blit(s.surf, s.rect)
                    if s.pitch == 1:       #In case our note is flat or sharp, we must draw the appropriate symbol.
                        s.draw_sharp(self)
                    elif s.pitch == -1:
                        s.draw_flat(self)
        
        #UPDATE THE POSITIONS OF ANYTHING BEING DRAGGED, THEN DRAW THEM
        if self.dragged_sprites:
            for s in self.dragged_sprites:  #This is implemented as a list of dragged sprites, but there should only be one dragged sprite at a time.
                s.goto_mouse()
                self.screen.blit(s.surf, s.rect)
            
        if self.playhead.dragged:
            self.playhead.goto(pygame.mouse.get_pos()[0])
        

        
        #DRAW WHAT NOTEVALUE IS UNDER THE MOUSE, AT THE TOP LEFT OF THE SCREEN
        row, col = self.calc_nearest_space()
        if (row >= 0) and (row < NOTERANGE):
            text = myfont.render(note_names[row], False, (0, 0, 0))
            self.screen.blit(text, (10,10))

      
        #print(pos)
        pygame.display.flip()
    
    
    #------------EVENT-DRIVEN FUNCTIONS, SUCH AS FOR CLICKING-----------------#
    def left_click_event(self):
        global NOTERANGE, BPM, MAXSONGLENGTH, MAXNOTESPERCELL, PADDING_X, PADDING_Y, SIGNATURE, LOOPING
        if self.menu_open:
            return
        pos = pygame.mouse.get_pos()
        #Create a new note when the note button is clicked
        spacing_x = self.get_note_spacing()
        pixels = self.get_total_pixels()
        if (not(self.playing)):
            for s in self.note_buttons:
                if s.rect.collidepoint(pos):
                    newnote = Note(s.get_id())
                    self.dragged_sprites.clear()
                    self.dragged_sprites.append(newnote)
                    self.play_note_sound(newnote,7)
                    break
           
            row, col = self.calc_nearest_space()
            if self.dragged_sprites:
                for s in self.dragged_sprites:
                    if (row >= 0) and (col >= 0) and (row < NOTERANGE):
                        if len(self.song_grid[col][row]) < MAXNOTESPERCELL:
                            already_exist = False
                            for note in self.song_grid[col][row]:
                                if note.get_id() == s.get_id():
                                    already_exist = True
                            if not(already_exist) and self.get_num_of_notes_at_timepoint(col) < NUMCHANNELS:
                                newnote = Note(s.get_id())
                                self.song_grid[col][row].append(newnote)
                                self.play_note_sound(newnote,row)
            else:
                 #Grabs a note from the song grid in order to move it around
                if (row >= 0) and (col >= 0) and (row < NOTERANGE):
                    pitch_notes = self.song_grid[col][row]
                    if pitch_notes:
                        s = pitch_notes[0]  #Defaulting to the first sprite. Will want to improve this later. See also the right_click_event
                        self.dragged_sprites.append(s)
                        pitch_notes.remove(s)
                        self.play_note_sound(s,row)
                #Otherwise, try dragging the playhead or scrollbar
                elif self.playhead.rect.collidepoint(pos):
                    self.playhead.dragged = True
                elif self.scrollbar.backdrop_rect.collidepoint(pos):
                    self.scrollbar.dragged = True
                elif (pos[1] < PADDING_Y+20) and (pos[1] > PADDING_Y-90):
                    row, col = self.calc_nearest_space()
                    if col >= 0:
                        self.current_timepoint = col
                        self.playhead.goto_timepoint(self.current_timepoint, spacing_x, self.scroll*pixels)
                        self.playhead.dragged = True
                elif self.savebutton.rect.collidepoint(pos):
                    self.file_save()
                elif self.loadbutton.rect.collidepoint(pos):
                    self.file_load()
                elif self.bpm_button.rect.collidepoint(pos):
                    self.bpm_button.click_increase(pos)
                elif self.sig_button.rect.collidepoint(pos):
                    if SIGNATURE == 4:
                        SIGNATURE = 3
                    elif SIGNATURE == 3:
                        SIGNATURE = 4
                    self.sig_button.update_appearance()
                elif self.loop_button.rect.collidepoint(pos):
                    if LOOPING:
                        LOOPING = False
                    else:
                        LOOPING = True
                    self.loop_button.update_appearance()

    def right_click_event(self):
        pos = pygame.mouse.get_pos()
        if not(self.playing):
            if self.dragged_sprites:
                self.dragged_sprites.clear()
            elif self.bpm_button.rect.collidepoint(pos) and not(self.menu_open):
                    self.bpm_button.click_decrease(pos)
            else:
                #The right click is used to make a note sharp or flat (if possible).
                row, col = self.calc_nearest_space()
                if (row >= 0) and (col >= 0) and (row < NOTERANGE):
                    pitch_notes = self.song_grid[col][row]
                    for s in pitch_notes:
                        #s = pitch_notes[0]
                        if (s.pitch == 0):  #if it starts as normal
                            if can_be_sharped(row):
                                s.pitch = 1
                                self.play_note_sound(s,row)
                            elif can_be_flatted(row):
                                s.pitch = -1
                                self.play_note_sound(s,row)
                        elif (s.pitch == 1):
                            if can_be_flatted(row):
                                s.pitch = -1
                                self.play_note_sound(s,row)
                            else:
                                s.pitch = 0
                                self.play_note_sound(s,row)
                        elif (s.pitch == -1):
                            s.pitch = 0
                            self.play_note_sound(s,row)

    def release_click_event(self):
        spacing_x = self.get_note_spacing()
        pixels = self.get_total_pixels()
        if not(self.playing):
            row, col = self.calc_nearest_space()
            if self.playhead.dragged:
                self.playhead.dragged = False
                if (col >= 0):
                    self.current_timepoint = col
                elif col == -1:
                    self.current_timepoint = 0
                elif col == -2:
                    self.current_timepoint = MAXSONGLENGTH
               # print(CURRENT_TIMEPOINT)
                self.playhead.goto_timepoint(self.current_timepoint, spacing_x, self.scroll*pixels)    

            if self.scrollbar.dragged:
                self.scrollbar.dragged = False
    
    def middle_click_event(self):
        global PADDING_X, PADDING_Y, SIGNATURE, ENDPOINT
        spacing_x = self.get_note_spacing()
        pixels = self.get_total_pixels()
        row, col = self.calc_nearest_space()
        if row >= 0 and col > 0:
            #self.end_marker = EndMarker(PADDING_X + col*spacing_x - (self.scroll*pixels), PADDING_Y, "paper_brown")
            ENDPOINT = col
            assert(ENDPOINT > 0)
            self.end_marker = EndMarker("paper_brown")
            self.end_marker.goto_timepoint(ENDPOINT, spacing_x, self.scroll*pixels)
        else:
            ENDPOINT = -1
            self.end_marker = None
         #line = Vertical_line(PADDING_X + (i*(2*SIGNATURE))*spacing_x - (self.scroll*pixels), PADDING_Y, 2, "paper_brown")



    def start_playing(self):
        global BPM, MAXSONGLENGTH, ENDPOINT, ADVANCE_BEAT
        if self.playhead.dragged:
            return
        self.playhead.reset_movetimer()
        if self.current_timepoint == MAXSONGLENGTH or self.current_timepoint == ENDPOINT:
            self.current_timepoint = 0
            self.scroll = 0
            self.scrollbar.update_x(self.scroll)
            self.playhead.move_timer = 0
        elif self.playhead.rect.x < 0:
            self.advance_scroll()
        self.playhead.move_timer = 0
        pixels = self.get_total_pixels()
        self.playing = True
        #pygame.event.post(pygame.event.Event(ADVANCE_BEAT))
        self.play_sounds()
        pygame.time.set_timer(ADVANCE_BEAT, (int((0.5*60/BPM)*1000)))
        self.playhead.goto_timepoint(self.current_timepoint, self.get_note_spacing(), self.scroll*pixels)
        #print((int(60/BPM)*100))

    def play_from_beginning(self):
        global ADVANCE_BEAT
        pygame.time.set_timer(ADVANCE_BEAT, 0)
        self.stop_playing()
        self.current_timepoint = 0
        self.scroll = 0
        self.scrollbar.update_x(self.scroll)
        self.start_playing()

    def stop_playing(self):
        global ADVANCE_BEAT
        pygame.mixer.stop()
        pygame.time.set_timer(ADVANCE_BEAT, 0)
        self.playing = False
        self.current_timepoint += 1
        self.playhead.goto_timepoint(self.current_timepoint, self.get_note_spacing(), self.scroll*self.get_total_pixels())

    def play_sounds(self):
        timepoint = self.song_grid[self.current_timepoint]
        for i in range(len(timepoint)):
            notes = timepoint[i]
            #print(len(notes))
            for note in notes:
                note.bouncing = True
                self.play_note_sound(note, i)
                #pygame.mixer.find_channel().play(sound_catalog[note.get_id()][i+modifier])
    
    def play_note_sound(self, note, pitch):
        modifier = 0
        if note.pitch == 1:
            modifier = 16
        if note.pitch == -1:
            modifier = 17
        
        channel = pygame.mixer.find_channel()  
        if not(channel):   #Prevents the game from crashing from trying to play too many sounds at once.
            pygame.mixer.stop()
            channel = pygame.mixer.find_channel()
        channel.play(sound_catalog[note.get_id()][pitch+modifier])

    #-------------HELPER FUNCTIONS-------------------------#
    def get_note_spacing(self):
        global SCREENWIDTH, PADDING_X, SIGNATURE
        return (SCREENWIDTH-2*(PADDING_X))/(self.zoom*(2*SIGNATURE))

    def calc_nearest_space(self):   #Finds the nearest song grid position to the mouse
        global PADDING_X, PADDING_Y, SCREENWIDTH, SCREENHEIGHT, NOTESPACING_Y, NOTESIZE, PADDING_X, MAXSONGLENGTH
        pos = pygame.mouse.get_pos()
        spacing_x = self.get_note_spacing()
        pixels = self.get_total_pixels()
        x, y = pos[0], pos[1]
        if (y < NOTESPACING_Y) or (y < 0) or (y > SCREENHEIGHT) or (x < 0) or (x > SCREENWIDTH):
            return (-1,-1)
        col = int((x+(self.scroll*pixels)-PADDING_X+(NOTESIZE/2))/spacing_x)
        row = int((y-PADDING_Y+(NOTESIZE/2))/NOTESPACING_Y)
        if (col < 0):
            return (-1, -1)      #negative numbers represents a position that is not allowed
        elif(col > MAXSONGLENGTH-1): 
            return (-2, -2)      #-1 is too far left, -2 is too far right
        return row, col

    def check_for_scroll(self):    #Checks if the playhead is close enough to the edge that the screen needs scrolling,
        global SCREENWIDTH, PADDING_X
        spacing_x = self.get_note_spacing()
        pixels = self.get_total_pixels()
        newx = PADDING_X - (self.scroll*pixels) + (self.current_timepoint)*spacing_x
        #newx = self.offset_x+((self.current_timepoint)*spacing_x)
        #print(newx)
        if (newx > SCREENWIDTH-200):
            self.advance_scroll()
            #game.scrollbar.update_x()
        #print("newx = ",newx,"OFFSET_X = ",OFFSET_X)
        #if (newx > SCREENWIDTH-PADDING_X)

    def advance_scroll(self):
        global PADDING_X, SCREENWIDTH
        spacing_x = self.get_note_spacing()
        pixels = self.get_total_pixels()
        #self.scroll += (SCREENWIDTH-2*PADDING_X)/pixels
        #self.scroll += (8*self.zoom / MAXSONGLENGTH)
        self.scroll = self.current_timepoint/MAXSONGLENGTH
        #self.offset_x -= (spacing_x*4*self.zoom)
        self.scrollbar.update_x(self.scroll)
        self.playhead.goto_timepoint(self.current_timepoint, spacing_x, self.scroll*pixels)
        #self.scrollbar.update_x(spacing_x, self.offset_x)
    
    def change_zoom(self, positive):
        changed = False
        if not(self.playing):
            if positive and self.zoom < MAXZOOM:
                changed = True
                self.zoom += 1
            elif not(positive) and self.zoom > MINZOOM:
                changed = True
                self.zoom -= 1
        if changed:
            spacing_x = self.get_note_spacing()
            pixels = self.get_total_pixels()
            self.playhead.goto_timepoint(self.current_timepoint, spacing_x, self.scroll*pixels)
            #self.offset_x = (self.offset_x/self.zoom)-(PADDING_X/DEFAULTZOOM)
            #self.offset_x = (self.offset_x/DEFAULTZOOM)*(self.zoom/4)
            #game.measure_lines.clear()
            #game.create_measure_lines()

    def get_total_pixels(self):
        return MAXSONGLENGTH*self.get_note_spacing()

    def file_save(self):
        self.menu_open = True
        f = filedialog.asksaveasfile(mode='w', defaultextension=".luz", filetypes=[("Song file","*.luz")])
        self.menu_open = False
        if f is None: # asksaveasfile return `None` if dialog closed with "cancel".
            return
        #text2save = str(text.get(1.0, END)) # starts from `1.0`, not `0.0`
        f.write(str(BPM)+'\n')
        f.write(str(LOOPING)+'\n')
        f.write(str(SIGNATURE)+'\n')
        f.write(str(ENDPOINT)+'\n')
        text = game.get_song_as_text()
        f.write(text)
        f.close() # `()` was missing.
    
    def file_load(self):
        global BPM, LOOPING, SIGNATURE, ENDPOINT
        self.menu_open = True
        self.song_grid.clear()
        self.create_song_grid()
        f = filedialog.askopenfile(defaultextension=".luz", filetypes=[("Song file","*.luz")])
        self.menu_open = False
        if f is None: # asksaveasfile return `None` if dialog closed with "cancel".
            return
        lines = f.readlines()
        BPM = int(lines[0].strip())
        self.bpm_button.update_appearance()
        LOOPING = (lines[1].strip() == "True")
        self.loop_button.update_appearance()
        SIGNATURE = int(lines[2].strip())
        self.sig_button.update_appearance()
        end = int(lines[3].strip())
        if end >= 1:
            ENDPOINT = end
            self.end_marker = EndMarker("paper_brown")
        else:
            self.end_marker = None
            ENDPOINT = -1
        #Remove the data we already used.      
        
        lines.remove(lines[0])
        lines.remove(lines[0])
        lines.remove(lines[0])
        lines.remove(lines[0])

        for tp in range(len(lines)):
            line = lines[tp]
            pitches = line.split(" ")
            pitches.remove('\n')
            for p in range(len(pitches)):
                if pitches[p]:
                    #notes_txt = [pitches[p][i:i+2] for i in range(0, len(pitches[p]),2)]
                    notes_txt = self.split_str(pitches[p])
                    for note_txt in notes_txt:
                        note = Note(int(note_txt[0]))
                        if note_txt[1] == 's':
                            note.pitch = 1
                        elif note_txt[1] == 'f':
                            note.pitch = -1
                        self.song_grid[tp][p].append(note)
        f.close()
        self.current_timepoint = 0
        self.scroll = 0
        self.scrollbar.update_x(self.scroll)
        self.playhead.goto_timepoint(0, self.get_note_spacing(), self.scroll*self.get_total_pixels())

    def get_song_as_text(self):
        text = ""
        for tp in range(MAXSONGLENGTH):
            for pitch in range(NOTERANGE):
                notes = self.song_grid[tp][pitch] #all the notes at a point
                for note in notes:
                    
                    text += str(note.get_id())
                    if note.pitch == 1:
                        text += "s"
                    elif note.pitch == -1:
                        text += "f"
                    else:
                        text += "n"
                text += " "
            text += "\n"
        return text
    
    def split_str(self, input):
        return_val = []
        new_str = ""
        for i in range(len(input)):
            ch = input[i]
            if (ord(ch) >= 48 and ord(ch) <= 57):
                new_str += input[i]
            else:
                return_val.append([new_str, input[i]])
                new_str = ""
        return return_val

    def at_end_of_song(self):
        return self.current_timepoint >= MAXSONGLENGTH-1 or self.current_timepoint == ENDPOINT-1
    
    def get_num_of_notes_at_timepoint(self, tp):
        num_notes = 0
        for pitch in self.song_grid[tp]:
            num_notes += len(pitch)
        return num_notes

    def easter_egg(self):
        newnote = Note(15)
        self.dragged_sprites.clear()
        self.dragged_sprites.append(newnote)
        self.play_note_sound(newnote,7)

#--------------------------MAIN GAMELOOP-----------------------------#
running = True 
pygame.display.set_caption("Owl Paint Composer")
screen = pygame.display.set_mode([SCREENWIDTH, SCREENHEIGHT])
game = GameManager(screen)

#pygame.mixer.init(frequency=44100, channels=NUMCHANNELS)

pygame.mixer.set_num_channels(NUMCHANNELS)

while running:
    game.redraw_screen()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                game.left_click_event()
            elif event.button == 2:
                game.middle_click_event()
            elif event.button == 3:
                game.right_click_event()
            elif event.button == 4:
                game.change_zoom(False)
            elif event.button == 5:
                game.change_zoom(True)
        if event.type == pygame.MOUSEBUTTONUP:
            game.release_click_event()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if not(game.playing):
                   game.start_playing()
                else:
                    game.stop_playing()
            elif event.key == pygame.K_DELETE:
                game.reset_song_grid()
            elif event.key == pygame.K_HOME:
                game.easter_egg()
            #elif event.key == pygame.K_l:
                #game.file_load()
        if event.type == PLAY_NOTES:
            game.play_notes()
        if event.type == ADVANCE_BEAT:
            if game.at_end_of_song():
                if LOOPING:
                    game.play_from_beginning()
                else:
                    game.stop_playing()
            else:
                game.check_for_scroll()
                game.current_timepoint += 1
                game.playhead.reset_movetimer()
                #game.playhead.goto_timepoint(game.current_timepoint, game.get_note_spacing(), game.scroll*game.get_total_pixels())
                game.play_sounds()
    clock.tick()
pygame.quit()
