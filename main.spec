# -*- mode: python ; coding: utf-8 -*-


block_cipher = None

note_imgs = [("./gfx/note_icons/*.png", "./gfx/note_icons")]
obj_imgs = [("./gfx/objects/*.png", "./gfx/objects")]
sounds = [
	("./notes/luz/*.wav","./notes/luz"),
	("./notes/king/*.wav","./notes/king"),
	("./notes/eda/*.wav","./notes/eda"),
	("./notes/hooty/*.wav","./notes/hooty"),
	("./notes/owlbert/*.wav","./notes/owlbert"),
	("./notes/amity/*.wav","./notes/amity"),
	("./notes/lilith/*.wav","./notes/lilith"),
	("./notes/willow/*.wav","./notes/willow"),
	("./notes/gus/*.wav","./notes/gus"),
	("./notes/raine/*.wav","./notes/raine"),
	("./notes/kikimora/*.wav","./notes/kikimora"),
	("./notes/hunter/*.wav","./notes/hunter"),
	("./notes/belos/*.wav","./notes/belos"),
	("./notes/vee/*.wav","./notes/vee"),
	("./notes/ghost/*.wav","./notes/ghost/"),
	("./notes/lego/*.wav","./notes/lego"),
]

exc_file = open("excluded.txt", "r")
excluded = [line.strip() for line in exc_file.readlines()]
exc_file.close()

a = Analysis(['main.py'],
             binaries=[],
             datas=note_imgs+obj_imgs+sounds,
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=excluded,
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,  
          [],
          name='Owl Paint Composer',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None, icon='./gfx/objects/logo.ico' )
