# Owl Paint Composer

Owl House-themed remake of the Mario Paint Composer using Pygame.

## Running

Can be run as a python script using ```python main.py```, or can be compiled to a standalone application using pyinstaller.

## Compilation

To compile, use ```python build.py```. This will ensure that only the required packages are included in the compilation.
